package org.adw.library.widgets.discreteseekbar.internal.drawable;

import ohos.agp.components.ComponentState;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.adw.library.widgets.discreteseekbar.internal.ColorStateList;

public class ThumbDrawable {
    private Paint paint = new Paint();
    public static final int DEFAULT_SIZE_DP = 12;
    private int mSize;
    private ColorStateList colorStateList;
    private DiscreteSeekBar discreteSeekBar;

    public ThumbDrawable(ColorStateList colorStateList, DiscreteSeekBar discreteSeekBar,int size){
        this.colorStateList = colorStateList;
        this.discreteSeekBar = discreteSeekBar;
        mSize = size;
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_STYLE);
    }
    public void doDraw(Canvas canvas, float centerX,float centerY){
        if(!discreteSeekBar.isEnabled()){
            paint.setColor(new Color(colorStateList.getColorForState(new int[]{ComponentState.COMPONENT_STATE_DISABLED},colorStateList.getDefaultColor())));
        }else if(discreteSeekBar.isPressed()){
            paint.setColor(new Color(colorStateList.getColorForState(new int[]{ComponentState.COMPONENT_STATE_PRESSED},colorStateList.getDefaultColor())));
        }else if(discreteSeekBar.isFocused()){
            paint.setColor(new Color(colorStateList.getColorForState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED},colorStateList.getDefaultColor())));
        }else {
            paint.setColor(new Color(colorStateList.getDefaultColor()));
        }
        float radius = (mSize / 2);
        canvas.drawCircle(centerX, centerY, radius, paint);
    }

    public void setColor(int color){
        colorStateList = new ColorStateList();
        colorStateList.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY},color);
        discreteSeekBar.invalidate();
    }

    public void setColorStateList(ColorStateList colorStateList){
        this.colorStateList = colorStateList;
        discreteSeekBar.invalidate();
    }
}
