/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.adw.library.widgets.discreteseekbar.internal;

import ohos.agp.components.ComponentState;

import java.util.HashMap;

/**
 * ColorStateList
 *
 */
public class ColorStateList {
    private HashMap<Integer, Integer> colorStateList = new HashMap<>();
    private int defaultColor = 0;

    /**
     * add state color
     *
     * @param states the state list
     * @param color the color
     */
    public void addState(int[] states, int color) {
        if (states != null && states.length > 0) {
            for (int i = 0; i < states.length; i++) {
                colorStateList.put(states[i], color);
                if (states[i] == ComponentState.COMPONENT_STATE_EMPTY) {
                    defaultColor = color;
                }
            }
        }
        if (states != null && states.length == 0) {
            defaultColor = color;
        }
    }

    /**
     * get color by state
     *
     * @param states the state list
     * @param color the default color
     * @return color int
     */
    public int getColorForState(int[] states, int color) {
        if (states != null && states.length > 0) {
            for (int i = 0; i < states.length; i++) {
                if (colorStateList.get(states[i]) != null) {
                    return colorStateList.get(states[i]);
                }
            }
        }
        if (states != null && states.length == 0) {
            return defaultColor;
        }
        return color;
    }

    /**
     * get default color
     *
     * @return color int
     */
    public int getDefaultColor() {
        return defaultColor;
    }
}
