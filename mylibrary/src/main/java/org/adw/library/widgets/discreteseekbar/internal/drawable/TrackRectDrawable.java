package org.adw.library.widgets.discreteseekbar.internal.drawable;

import ohos.agp.components.ComponentState;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.adw.library.widgets.discreteseekbar.internal.ColorStateList;

public class TrackRectDrawable {
    private Paint paint = new Paint();
    private ColorStateList colorStateList;
    private DiscreteSeekBar discreteSeekBar;

    public TrackRectDrawable(ColorStateList colorStateList, DiscreteSeekBar discreteSeekBar){
        this.colorStateList = colorStateList;
        this.discreteSeekBar = discreteSeekBar;
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_STYLE);
    }

    public void doDraw(Canvas canvas, RectFloat rectFloat){
        if(!discreteSeekBar.isEnabled()){
            paint.setColor(new Color(colorStateList.getColorForState(new int[]{ComponentState.COMPONENT_STATE_DISABLED},colorStateList.getDefaultColor())));
        }else if(discreteSeekBar.isPressed()){
            paint.setColor(new Color(colorStateList.getColorForState(new int[]{ComponentState.COMPONENT_STATE_PRESSED},colorStateList.getDefaultColor())));
        }else if(discreteSeekBar.isFocused()){
            paint.setColor(new Color(colorStateList.getColorForState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED},colorStateList.getDefaultColor())));
        }else {
            paint.setColor(new Color(colorStateList.getDefaultColor()));
        }
        canvas.drawRect(rectFloat,paint);
    }

    public void setColor(int color){
        colorStateList = new ColorStateList();
        colorStateList.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY},color);
        discreteSeekBar.invalidate();
    }

    public void setColorStateList(ColorStateList colorStateList){
        this.colorStateList = colorStateList;
        discreteSeekBar.invalidate();
    }
}
