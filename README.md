# demo

<img src="art/demo.gif" width="40%"/>

# discreteseekbar

# 如何导入?

```
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:discreteSeekBar:1.0.1'
}
```

# 如何使用

## xml中加入：
```xml
    <org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
        ohos:margin="5vp"
        ohos:id="$+id:discreteSeekBar"
        ohos:height="40vp"
        ohos:width="match_parent"
        app:dsb_indicatorPopupEnabled="false"
        app:dsb_min="-5"
        app:dsb_max="5"
        app:dsb_value="-2"
        />

    <org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
        ohos:margin="5vp"
        ohos:height="40vp"
        ohos:width="match_parent"
        app:dsb_allowTrackClickToDrag="false"
        app:dsb_progressColor="#FFFF8877"
        app:dsb_rippleColor="#FF7788FF"
        app:dsb_indicatorColor="#FFFF8877"
        app:dsb_indicatorPressedColor="#FFFFFFFF"
        app:dsb_indicatorFormatter="\\o/ %d"
        app:dsb_indicatorTextColor="#ff000000"
        app:dsb_indicatorTextSize="9vp"
        app:dsb_indicatorTextShadowColor="#FFFF0000"
        app:dsb_indicatorTextShadowRadius="4vp"
        app:dsb_indicatorTextShadowDx="1vp"
        app:dsb_indicatorTextShadowDy="3vp"
        app:dsb_indicator_textPadding="10vp"
        />
```

## java调用：
```java

    discreteSeekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {
                //value是进度值可以通过return不同的值使气泡显示不同
                return value;
            }
        });
    discreteSeekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                //value变化值
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });
```

# API:
## class DiscreteSeekBar
**public void setIndicatorFormatter(String formatter)**
- description: set indicator formatter

**public void setNumericTransformer(NumericTransformer transformer)**
- description: set numeric transformer

**public NumericTransformer getNumericTransformer()**
- description: get numeric transformer

**public void setMax(int max)**
- description: set progress max

**public int getMax()**
- description: get progress max

**public void setMin(int min)**
- description: set progress min

**public int getMin()**
- description: get progress min

**public void setProgress(int progress)**
- description: set progress

**public int getProgress()**
- description: get progress

**public void setOnProgressChangeListener(OnProgressChangeListener listener)**
- description: set progress change listener

**public void setThumbColor(int thumbColor, int indicatorColor, int indicatorPressedColor)**
- description: set thumb color and indicator color

**public void setThumbColor(ColorStateList thumbColorStateList, int indicatorColor, int indicatorPressedColor)**
- description: set thumb color with ColorStateList and indicator color

**public void setDefaultBackgroundColor(int defaultBackgroundColor)**
- description: set default background color

**public void setScrubberColor(int color)**
- description: set scrubber color

**public void setScrubberColor(ColorStateList colorStateList)**
- description: set scrubber color with ColorStateList

**public void setRippleColor(int color)**
- description: set ripple color

**public void setRippleColor(ColorStateList colorStateList)**
- description: set ripple color with ColorStateList

**public void setTrackColor(int color)**
- description: set track color

**public void setTrackColor(ColorStateList colorStateList)**
- description: set track color with ColorStateList

**public void setIndicatorPopupEnabled(boolean enabled)**
- description: set indicator enanbled

# AttrSet:

|name|format|description|
|:---:|:---:|:---:|
| dsb_topBarHeight | dimension | set top bar height default 43vp
| dsb_allowTrackClickToDrag | boolean | set allow track when click
| dsb_indicatorPopupEnabled | boolean | set show indicator popup
| dsb_trackHeight | dimension | set track height
| dsb_scrubberHeight | dimension | set scrubber height
| dsb_indicatorTextSize | dimension | set indicator text size
| dsb_thumbSize | dimension | set thumb size
| dsb_indicatorSeparation | dimension | set indicator separation
| dsb_max | int | set progress max
| dsb_min | int | set progress min
| dsb_value | int | set progress default value
| dsb_indicatorFormatter | string | set indicator text formatter
| dsb_trackColor | color | set track default color
| dsb_trackPressedColor | color | set track pressed color
| dsb_trackFocusedColor | color | set track focused color
| dsb_trackDisabledColor | color | set track disabled color
| dsb_progressColor | color | set progress default color
| dsb_progressPressedColor | color | set progress pressed color
| dsb_progressFocusedColor | color | set progress focused color
| dsb_progressDisabledColor | color | set progress disabled color
| dsb_rippleColor | color | set ripple default color
| dsb_ripplePressedColor | color | set ripple pressed color
| dsb_rippleFocusedColor | color | set ripple focused color
| dsb_rippleDisabledColor | color | set ripple disabled color
| dsb_indicator_textPadding | dimension | set indicator text padding
| dsb_indicatorColor | color | set indicator defualt color
| dsb_indicatorPressedColor | color | set indicator pressed color
| dsb_indicatorElevation | dimension | set indicator elevation width default 4vp
| dsb_indicatorTextColor | color | set indicator text color
| dsb_indicatorTextShadowColor | color | set indicator text shadow color
| dsb_indicatorTextShadowRadius | dimension | set indicator text shadow radius
| dsb_indicatorTextShadowDx | dimension | set indicator text shadow dx
| dsb_indicatorTextShadowDy | dimension | set indicator text shadow dy




##License
```
Copyright 2014 Gustavo Claramunt (Ander Webbs)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```